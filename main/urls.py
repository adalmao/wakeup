"""src URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from main import views

urlpatterns = [
    url(r'^$',views.home2,name='home'),
    url(r'^contact/$',views.contact,name='contact'),
    url(r'^service/detail/(?P<service_pk>.+)/$',views.service_detail,name='service_detail'),
    url(r'^post/detail/(?P<post_pk>.+)/$',views.post_detail,name='post_detail'),
    url(r'^getservices/$',views.get_services,name='get_services'),
    url(r'^getcontries/$',views.get_contries,name='get_contries'),
    url(r'^getknow/$',views.get_know,name='get_know'),
    url(r'^getstates/(?P<country_pk>.+)/$',views.get_states,name='get_states'),
]
