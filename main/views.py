from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render,redirect,get_object_or_404
from panel.models import *
from panel.forms import CommentForm
from django.http import HttpResponse,JsonResponse,HttpResponseRedirect
import json
from src.settings import APPSELOM_URL,AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY,AWS_STORAGE_BUCKET_NAME,TOKEN_APSELOM,LINK_INTRANET
import boto3



# Create your views here.

def home(request):
	return render(request,'main/home.html',locals())

def get_services(request):
	s3_session = boto3.Session(aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
	bucket_object = s3_session.resource('s3').Bucket(AWS_STORAGE_BUCKET_NAME).Object("static/main/json/programs.json")
	content = bucket_object.get()['Body'].read()
	json_content = json.loads(content)
	return HttpResponse(json.dumps(json_content),content_type="application/json",status='200')
def get_contries(request):
	s3_session = boto3.Session(aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
	bucket_object = s3_session.resource('s3').Bucket(AWS_STORAGE_BUCKET_NAME).Object("static/main/json/contries_get.json")
	content = bucket_object.get()['Body'].read()
	json_content = json.loads(content)
	return HttpResponse(json.dumps(json_content),content_type="application/json",status='200')
def get_know(request):
	s3_session = boto3.Session(aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
	bucket_object = s3_session.resource('s3').Bucket(AWS_STORAGE_BUCKET_NAME).Object("static/main/json/conociste.json")
	content = bucket_object.get()['Body'].read()
	json_content = json.loads(content)
	return HttpResponse(json.dumps(json_content),content_type="application/json",status='200')
def get_states(request,country_pk):
	s3_session = boto3.Session(aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
	bucket_object = s3_session.resource('s3').Bucket(AWS_STORAGE_BUCKET_NAME).Object("static/main/json/contries.json")
	content = bucket_object.get()['Body'].read()
	json_content=[]
	content= json.loads(content)
	for c in content:
		if c['country_id']==int(country_pk):
			json_content.append(c)
	return HttpResponse(json.dumps(json_content),content_type="application/json",status='200')

def home2(request):
	title_page='Apasionados en los logros personales, académicos y profesionales'
	appselom_url=APPSELOM_URL
	token=TOKEN_APSELOM
	sliders=Slider.objects.filter(is_active=True).order_by('created_at')
	services=Service.objects.filter(is_active=True).order_by('created_at')
	pilars=Pilar.objects.all().order_by('created_at')
	testimonies=Testimony.objects.exclude(is_primary=True).order_by('created_at')
	options_aux=OptionChoise.objects.filter(is_active=True).order_by('created_at')
	posts=Post.objects.all().order_by('created_at')
	options=[]
	cont=1
	intranet=LINK_INTRANET
	testimony_principal=None
	try:
		testimony_principal=Testimony.objects.get(is_primary=True)
	except Testimony.DoesNotExist:
		pass
	testimony_phone=[]
	
	for t in testimonies:
		testimony_phone.append(t)
	if testimony_principal:
		testimony_phone.append(testimony_principal)
	for o in options_aux:
		if cont == options_aux.count() and options_aux.count()%2 !=0:
			o.is_last=True
		options.append(o)
		cont+=1
	members=Team.objects.all()
	configuration=Configuration.objects.all()
	if len(configuration) > 1 or len(configuration) <1:
		for c in configuration:
			c.delete()
		configuration=Configuration()
		configuration.save()
	else:
		configuration=configuration.first()
	atentionhours=AtentionHours.objects.all()
	contactdetails=ContactDetail.objects.all()
	locationdetails=LocationDetail.objects.all()
	return render(request,'main/home2.html',locals())
def service_detail(request,service_pk):
	title_page='Apasionados en los logros personales, académicos y profesionales'
	services=Service.objects.filter(is_active=True)
	service=get_object_or_404(Service,pk=service_pk)
	try:
		course=Course.objects.get(service__pk=service_pk)
	except Course.DoesNotExist:
		course=None
	if course:
		objetives=CourseObjetive.objects.filter(course=course)
		show_objetives=True
	else:
		objetives=None
		show_objetives=False
	intranet=LINK_INTRANET
	configuration=Configuration.objects.all()
	if len(configuration) > 1 or len(configuration) <1:
		for c in configuration:
			c.delete()
		configuration=Configuration()
		configuration.save()
	else:
		configuration=configuration.first()
	atentionhours=AtentionHours.objects.all()
	contactdetails=ContactDetail.objects.all()
	locationdetails=LocationDetail.objects.all()
	return render(request,'main/service/detail.html',locals())

def get_childs(c):
	comments=Comment.objects.filter(father__pk=c.pk)
	if comments.count()<=0:
		return False
	return comments
def get_comments(post):
	comments=Comment.objects.filter(post=post,father__isnull=True)
	for c in comments:
		c.childs=get_childs(c)
	return comments

@csrf_exempt
def post_detail(request,post_pk):
	title_page='Apasionados en los logros personales, académicos y profesionales'
	post_description='WakeUp Educación & Desarrollo'
	post=get_object_or_404(Post,pk=post_pk)
	post_title=post.title
	post_image=post.file_data.url
	if request.POST:
		data=json.loads(request.POST.get('form') or None)
		child=json.loads(request.POST.get('child') or None)
		if child:
			comment_pk=json.loads(request.POST.get('comment') or None)
			comment=get_object_or_404(Comment,pk=comment_pk)
			form=CommentForm(data)
			if form.is_valid():
				comment_new=form.save(commit=False)
				comment_new.post=post
				comment_new.father=comment
				comment_new.save()
				data['post']=post.pk
				data['success'] = True
				return HttpResponse(json.dumps(data),content_type="application/json",status='200')
			else:
				response=JsonResponse({"errors":form.errors,"error":"Revise el formulario"})
				response.status_code=403
				return response
		else:
			form=CommentForm(data)
			if form.is_valid():
				comment_new=form.save(commit=False)
				comment_new.post=post
				comment_new.save()
				data['post']=post.pk
				data['success'] = True
				return HttpResponse(json.dumps(data),content_type="application/json",status='200')
			else:
				response=JsonResponse({"errors":form.errors,"error":"Revise el formulario"})
				response.status_code=403
				return response
	services=Service.objects.filter(is_active=True)
	comments=get_comments(post)
	intranet=LINK_INTRANET
	configuration=Configuration.objects.all()
	if len(configuration) > 1 or len(configuration) <1:
		for c in configuration:
			c.delete()
		configuration=Configuration()
		configuration.save()
	else:
		configuration=configuration.first()
	count_coments=Comment.objects.filter(post=post).count()
	atentionhours=AtentionHours.objects.all()
	contactdetails=ContactDetail.objects.all()
	locationdetails=LocationDetail.objects.all()
	return render(request,'main/post/item.html',locals())
def contact(request):
	title_page='Apasionados en los logros personales, académicos y profesionales'
	appselom_url=APPSELOM_URL
	token=TOKEN_APSELOM
	intranet=LINK_INTRANET
	services=Service.objects.filter(is_active=True)
	configuration=Configuration.objects.all()
	if len(configuration) > 1 or len(configuration) <1:
		for c in configuration:
			c.delete()
		configuration=Configuration()
		configuration.save()
	else:
		configuration=configuration.first()
	atentionhours=AtentionHours.objects.all()
	contactdetails=ContactDetail.objects.all()
	locationdetails=LocationDetail.objects.all()
	return render(request,'main/contact.html',locals())

