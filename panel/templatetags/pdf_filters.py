from django import template
	
register = template.Library()

@register.filter
def get64(url):
    """
    Method returning base64 image data instead of URL
    """

    return url