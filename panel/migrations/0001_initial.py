# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-12-17 09:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Slider',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Titulo')),
                ('subtitle', models.TextField(blank=True, null=True, verbose_name='Descripcion')),
                ('is_imagen', models.BooleanField(default=True, verbose_name='Es imagen? (No marcar si va a subir un video)')),
                ('file_data', models.FileField(null=True, upload_to='sliders/', verbose_name='Multimedia')),
                ('is_active', models.BooleanField(default=True, verbose_name='Activo')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
