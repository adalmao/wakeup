# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2020-01-14 01:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('panel', '0019_post_file_icon'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='category',
            field=models.CharField(default=1, max_length=255, verbose_name='Categoria'),
            preserve_default=False,
        ),
    ]
