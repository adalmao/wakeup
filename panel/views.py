from django.shortcuts import render,redirect,get_object_or_404
from django.core.urlresolvers import reverse

from django.contrib.auth.decorators import login_required
from panel.forms import *
from panel.models import *
# Create your views here.
@login_required
def home(request):
	return render(request,'panel/home.html',locals())
@login_required
def slider_list(request):
	sliders=Slider.objects.all()
	return render(request,'panel/slider/list.html',locals())
@login_required
def slider_new(request):
	title='Crear Slider'
	if request.POST:
		form=SliderForm(request.POST,request.FILES)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:slider_list'))
		else:
			message="Revisa la informacion"
	else:
		form=SliderForm()
	return render(request,'panel/hooks/form.html',locals())
@login_required
def slider_edit(request,slider_pk):
	slider=Slider.objects.get(pk=slider_pk)
	title='Editar Slider {0} - {1}'.format(slider.pk,slider.title)
	if request.POST:
		form=SliderForm(request.POST,request.FILES,instance=slider)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:slider_list'))
		else:
			message='Revisa la informacion'
	else:
		form=SliderForm(instance=slider)
	return render(request,'panel/hooks/form.html',locals())

@login_required
def slider_delete(request,slider_pk):
	slider=Slider.objects.get(pk=slider_pk)
	slider.delete()
	return redirect(reverse('panel:slider_list'))
@login_required
def service_list(request):
	services=Service.objects.all()
	return render(request,'panel/service/list.html',locals())
@login_required
def service_new(request):
	title='Crear Servicio'
	if request.POST:
		form=ServiceForm(request.POST,request.FILES)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:service_list'))
		else:
			message="Revisa la informacion"
	else:
		form=ServiceForm()
	return render(request,'panel/hooks/form.html',locals())
@login_required
def service_edit(request,service_pk):
	service=Service.objects.get(pk=service_pk)
	title='Editar Servicio {0} - {1}'.format(service.pk,service.title)
	if request.POST:
		form=ServiceForm(request.POST,request.FILES,instance=service)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:service_list'))
		else:
			message='Revisa la informacion'
	else:
		form=ServiceForm(instance=service)
	return render(request,'panel/hooks/form.html',locals())

@login_required
def service_delete(request,service_pk):
	service=Service.objects.get(pk=service_pk)
	service.delete()
	return redirect(reverse('panel:service_list'))
@login_required
def service_detail(request,service_pk):
	service=get_object_or_404(Service,pk=service_pk)
	title='Detalle Servicio {0}'.format(service.title)
	try:
		course=Course.objects.get(service__pk=service_pk)
	except Course.DoesNotExist:
		course=None
	if request.POST:
		if course:
			form=CourseForm(request.POST,request.FILES,instance=course)
		else:
			form=CourseForm(request.POST,request.FILES)
		if form.is_valid():
			if course:
				form.save()
			else:
				course=form.save(commit=False)
				course.service=service
				course.save()
			return redirect(reverse('panel:service_detail',kwargs={'service_pk':service.pk}))
		else:
			message='Revisa la informacion'
	else:
		if course:
			form=CourseForm(instance=course)
		else:
			form=CourseForm()
	if course:
		objetives=CourseObjetive.objects.filter(course=course)
		show_table=True
	else:
		objetives=None
		show_table=False
	return render(request,'panel/service/service_detail.html',locals())
@login_required
def pilar_list(request):
	pilars=Pilar.objects.all()
	return render(request,'panel/pilar/list.html',locals())
@login_required
def pilar_new(request):
	title='Crear Pilar de la empresa'
	if request.POST:
		form=PilarForm(request.POST,request.FILES)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:pilar_list'))
		else:
			message="Revisa la informacion"
	else:
		form=PilarForm()
	return render(request,'panel/hooks/form.html',locals())
@login_required
def pilar_edit(request,pilar_pk):
	pilar=Pilar.objects.get(pk=pilar_pk)
	title='Editar Pilar de la empresa {0} '.format(pilar.pk)
	if request.POST:
		form=PilarForm(request.POST,request.FILES,instance=pilar)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:pilar_list'))
		else:
			message='Revisa la informacion'
	else:
		form=PilarForm(instance=pilar)
	return render(request,'panel/hooks/form.html',locals())

@login_required
def pilar_delete(request,pilar_pk):
	pilar=Pilar.objects.get(pk=pilar_pk)
	pilar.delete()
	return redirect(reverse('panel:pilar_list'))
@login_required
def option_list(request):
	options=OptionChoise.objects.all().order_by('created_at')
	return render(request,'panel/option/list.html',locals())
@login_required
def option_new(request):
	title='Crear Opcion para elegirnos'
	if request.POST:
		form=OptionForm(request.POST,request.FILES)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:option_list'))
		else:
			message="Revisa la informacion"
	else:
		form=OptionForm()
	return render(request,'panel/hooks/form.html',locals())
@login_required
def option_edit(request,option_pk):
	option=OptionChoise.objects.get(pk=option_pk)
	title='Editar Opcion para elegirnos{0} -{1} '.format(option.pk,option.title)
	if request.POST:
		form=OptionForm(request.POST,request.FILES,instance=option)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:option_list'))
		else:
			message='Revisa la informacion'
	else:
		form=OptionForm(instance=option)
	return render(request,'panel/hooks/form.html',locals())

@login_required
def option_delete(request,option_pk):
	option=OptionChoise.objects.get(pk=option_pk)
	option.delete()
	return redirect(reverse('panel:option_list'))


@login_required
def objetive_new(request,course_pk):
	course=Course.objects.get(pk=course_pk)
	title='Crear Objetivo'
	if request.POST:
		form=CourseObjetiveForm(request.POST,request.FILES)
		if form.is_valid():
			objetive=form.save(commit=False)
			objetive.course=course
			objetive.save()
			return redirect(reverse('panel:service_detail',kwargs={'service_pk':course.service.pk}))
		else:
			message="Revisa la informacion"
	else:
		form=CourseObjetiveForm()
	return render(request,'panel/hooks/form.html',locals())
@login_required
def objetive_edit(request,course_pk,objetive_pk):
	objetive=CourseObjetive.objects.get(pk=objetive_pk)
	course=Course.objects.get(pk=course_pk)

	title='Editar Objetivo'
	if request.POST:
		form=CourseObjetiveForm(request.POST,request.FILES,instance=objetive)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:service_detail',kwargs={'service_pk':course.service.pk}))
		else:
			message='Revisa la informacion'
	else:
		form=CourseObjetiveForm(instance=objetive)
	return render(request,'panel/hooks/form.html',locals())

@login_required
def objetive_delete(request,course_pk,objetive_pk):
	course=Course.objects.get(pk=course_pk)	
	objetive=CourseObjetive.objects.get(pk=objetive_pk)
	objetive.delete()
	return redirect(reverse('panel:service_detail',kwargs={'service_pk':course.service.pk}))
@login_required
def configuration(request):
	configuration=Configuration.objects.all()
	if len(configuration) > 1 or len(configuration) <1:
		for c in configuration:
			c.delete()
		configuration=Configuration()
		configuration.save()
	else:
		configuration=configuration.first()
	atentionhours=AtentionHours.objects.all()
	contactdetails=ContactDetail.objects.all()
	locationdetails=LocationDetail.objects.all()
	return render(request,'panel/configuration.html',locals())

@login_required 
def configuration_stats(request):
	title="Edicion de valores de iniciales"
	configuration=Configuration.objects.all()
	if len(configuration) > 1 or len(configuration) <1:
		for c in configuration:
			c.delete()
		configuration=Configuration()
		configuration.save()
	else:
		configuration=configuration.first()
	if request.POST:
		form=ConfigurationForm(request.POST,request.FILES,instance=configuration)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:configuration'))
		else:
			message="Revise la informacion"
	else:
		form=ConfigurationForm(instance=configuration)
	return render(request,'panel/hooks/form.html',locals())
@login_required 
def configuration_rss(request):
	title="Edicion de Redes Sociales"
	configuration=Configuration.objects.all()
	if len(configuration) > 1 or len(configuration) <1:
		for c in configuration:
			c.delete()
		configuration=Configuration()
		configuration.save()
	else:
		configuration=configuration.first()
	if request.POST:
		form=ConfigurationRSSForm(request.POST,request.FILES,instance=configuration)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:configuration'))
		else:
			message="Revise la informacion"
	else:
		form=ConfigurationRSSForm(instance=configuration)
	return render(request,'panel/hooks/form.html',locals())

@login_required
def testimony_list(request):
	testimonies=Testimony.objects.all()
	return render(request,'panel/testimony/list.html',locals())
@login_required
def testimony_new(request):
	title='Crear Testimonio de la empresa'
	if request.POST:
		form=TestimonyForm(request.POST,request.FILES)
		if form.is_valid():
			testimony=form.save()
			if testimony.is_primary:
				ts=Testimony.objects.filter(is_primary=True)
				ts=ts.exclude(pk=testimony.pk)
				for t in ts:
					t.is_primary=False
					t.save()
			return redirect(reverse('panel:testimony_list'))
		else:
			message="Revisa la informacion"
	else:
		form=TestimonyForm()
	return render(request,'panel/hooks/form.html',locals())
@login_required
def testimony_edit(request,testimony_pk):
	testimony=Testimony.objects.get(pk=testimony_pk)
	title='Editar Testimonio de la empresa {0} '.format(testimony.pk)
	if request.POST:
		form=TestimonyForm(request.POST,request.FILES,instance=testimony)
		if form.is_valid():
			testimony=form.save()
			if testimony.is_primary:
				ts=Testimony.objects.filter(is_primary=True)
				ts=ts.exclude(pk=testimony.pk)
				for t in ts:
					t.is_primary=False
					t.save()
			return redirect(reverse('panel:testimony_list'))
		else:
			message='Revisa la informacion'
	else:
		form=TestimonyForm(instance=testimony)
	return render(request,'panel/hooks/form.html',locals())

@login_required
def testimony_delete(request,testimony_pk):
	testimony=Testimony.objects.get(pk=testimony_pk)
	testimony.delete()
	return redirect(reverse('panel:testimony_list'))

@login_required
def team_list(request):
	members=Team.objects.all()
	return render(request,'panel/team/list.html',locals())
@login_required
def team_new(request):
	title='Crear Miembro de la empresa'
	if request.POST:
		form=TeamForm(request.POST,request.FILES)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:team_list'))
		else:
			message="Revisa la informacion"
	else:
		form=TeamForm()
	return render(request,'panel/hooks/form.html',locals())
@login_required
def team_edit(request,team_pk):
	member=Team.objects.get(pk=team_pk)
	title='Editar Miembro de la empresa {0} '.format(member.pk)
	if request.POST:
		form=TeamForm(request.POST,request.FILES,instance=member)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:team_list'))
		else:
			message='Revisa la informacion'
	else:
		form=TeamForm(instance=member)
	return render(request,'panel/hooks/form.html',locals())

@login_required
def team_delete(request,team_pk):
	member=Team.objects.get(pk=team_pk)
	member.delete()
	return redirect(reverse('panel:team_list'))
@login_required
def post_list(request):
	posts=Post.objects.all()
	return render(request,'panel/post/list.html',locals())
@login_required
def post_new(request):
	title='Crear Post'
	if request.POST:
		form=PostForm(request.POST,request.FILES)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:post_list'))
		else:
			message="Revisa la informacion"
	else:
		form=PostForm()
	return render(request,'panel/hooks/form.html',locals())
@login_required
def post_edit(request,post_pk):
	post=Post.objects.get(pk=post_pk)
	title='Editar Post de la empresa {0} '.format(post.pk)
	if request.POST:
		form=PostForm(request.POST,request.FILES,instance=post)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:post_list'))
		else:
			message='Revisa la informacion'
	else:
		form=PostForm(instance=post)
	return render(request,'panel/hooks/form.html',locals())

@login_required
def post_delete(request,post_pk):
	post=Post.objects.get(pk=post_pk)
	post.delete()
	return redirect(reverse('panel:post_list'))
@login_required
def comment_list(request,post_pk):
	post=get_object_or_404(Post,pk=post_pk)
	comments=Comment.objects.filter(post__pk=post.pk,father__isnull=True)
	return render(request,'panel/post/comments.html',locals())
@login_required
def comment_new(request,post_pk):
	post=get_object_or_404(Post,pk=post_pk)
	title='Crear Comentario'
	if request.POST:
		form=CommentForm(request.POST,request.FILES)
		if form.is_valid():
			comment=form.save(commit=False)
			comment.post=post
			comment.save()
			return redirect(reverse('panel:comment_list',kwargs={'post_pk':post.pk}))
		else:
			message="Revisa la informacion"
	else:
		form=CommentForm()
	return render(request,'panel/hooks/form.html',locals())
@login_required
def comment_edit(request,comment_pk):
	comment=Comment.objects.get(pk=commet_pk)
	title='Editar Comentario del Post {0} '.format(comment.post.pk)
	if request.POST:
		form=CommentForm(request.POST,request.FILES,instance=comment)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:comment_list',kwargs={'post_pk':comment.post.pk}))
		else:
			message='Revisa la informacion'
	else:
		form=CommentForm(instance=comment)
	return render(request,'panel/hooks/form.html',locals())

@login_required
def comment_delete(request,comment_pk):
	comment=Comment.objects.get(pk=comment_pk)
	post_pk=comment.post.pk
	comment.delete()
	return redirect(reverse('panel:comment_list',kwargs={'post_pk':post_pk}))
@login_required
def comment_child(request,comment_pk):
	comment=Comment.objects.get(pk=comment_pk)
	title='Agregar Comentario Hijo del Post {0} '.format(comment.post.pk)
	if request.POST:
		form=CommentForm(request.POST,request.FILES)
		if form.is_valid():
			comment_new=form.save(commit=False)
			comment_new.post=comment.post
			comment_new.father=comment
			comment_new.save()
			return redirect(reverse('panel:comment_child_list',kwargs={'comment_pk':comment_pk}))
		else:
			message='Revisa la informacion'
	else:
		form=CommentForm()
	return render(request,'panel/hooks/form.html',locals())
@login_required
def comment_child_list(request,comment_pk):
	comment=Comment.objects.get(pk=comment_pk)
	comments=Comment.objects.filter(father=comment.pk)
	return render(request,'panel/post/comments_childs.html',locals())

@login_required
def atention_new(request):
	title='Crear Horario de Atencion'
	if request.POST:
		form=AtentionHoursForm(request.POST,request.FILES)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:configuration'))
		else:
			message="Revisa la informacion"
	else:
		form=AtentionHoursForm()
	return render(request,'panel/hooks/form.html',locals())
@login_required
def atention_edit(request,atention_pk):
	post=AtentionHours.objects.get(pk=atention_pk)
	title='Editar Horario de Atencion'
	if request.POST:
		form=AtentionHoursForm(request.POST,request.FILES,instance=post)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:configuration'))
		else:
			message='Revisa la informacion'
	else:
		form=AtentionHoursForm(instance=post)
	return render(request,'panel/hooks/form.html',locals())

@login_required
def atention_delete(request,atention_pk):
	post=AtentionHours.objects.get(pk=atention_pk)
	post.delete()
	return redirect(reverse('panel:configuration'))
@login_required
def contact_new(request):
	title='Crear Detalle de Contactanos'
	if request.POST:
		form=ContactDetailForm(request.POST,request.FILES)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:configuration'))
		else:
			message="Revisa la informacion"
	else:
		form=ContactDetailForm()
	return render(request,'panel/hooks/form.html',locals())
@login_required
def contact_edit(request,contact_pk):
	post=ContactDetail.objects.get(pk=contact_pk)
	title='Editar Detalle de Contactanos'
	if request.POST:
		form=ContactDetailForm(request.POST,request.FILES,instance=post)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:configuration'))
		else:
			message='Revisa la informacion'
	else:
		form=ContactDetailForm(instance=post)
	return render(request,'panel/hooks/form.html',locals())

@login_required
def contact_delete(request,contact_pk):
	post=ContactDetail.objects.get(pk=contact_pk)
	post.delete()
	return redirect(reverse('panel:configuration'))
@login_required
def location_new(request):
	title='Crear Detalla de Ubicacion'
	if request.POST:
		form=LocationDetailForm(request.POST,request.FILES)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:configuration'))
		else:
			message="Revisa la informacion"
	else:
		form=LocationDetailForm()
	return render(request,'panel/hooks/form.html',locals())
@login_required
def location_edit(request,location_pk):
	post=LocationDetail.objects.get(pk=location_pk)
	title='Editar Detalle de Ubicacion'
	if request.POST:
		form=LocationDetailForm(request.POST,request.FILES,instance=post)
		if form.is_valid():
			form.save()
			return redirect(reverse('panel:configuration'))
		else:
			message='Revisa la informacion'
	else:
		form=LocationDetailForm(instance=post)
	return render(request,'panel/hooks/form.html',locals())

@login_required
def location_delete(request,location_pk):
	post=LocationDetail.objects.get(pk=location_pk)
	post.delete()
	return redirect(reverse('panel:configuration'))
