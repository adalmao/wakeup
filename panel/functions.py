from django.template.loader import get_template
from django.contrib.sites.shortcuts import get_current_site
from main.models import *
import base64
from django.http import HttpResponse,JsonResponse
import json
def add_form_control_class(fields):
    for f in fields:
        fields[f].widget.attrs.update({'class': 'form-control'})
def add_field_control_class(field):
    field.widget.attrs.update({'class': 'form-control'})
def add_field_select_class(field):
    field.widget.attrs.update({'class': 'select2'})

# def add_class(form, fields):
#     for field in fields:
#         form.fields[field].widget.attrs.update(
#             {
#                 "class": 'form-control',
#                 # 'placeholder' : form.fields[field].label
#             }
#         )
import urllib
from django.core.urlresolvers import reverse

def build_url(*args, **kwargs):
    get = kwargs.pop('get', {})
    url = reverse(*args, **kwargs)
    if get:
        url += '?' + urllib.parse.urlencode(get)
    return url

def add_form_control_datepicker_class(form, fields):
    for f in fields:
        form.fields[f].widget.attrs.update({'class': 'form-control datepicker'})


def add_form_text(form, fields):
    for f in fields:
        form.fields[f].widget.attrs.update({'type': 'text'})


def add_form_onlyread(form, fields):
    for f in fields:
        form.fields[f].widget.attrs.update({'readonly': 'true'})


def add_form_required(fields):
    for f in fields:
        fields[f].widget.attrs.update({'required': 'true'})


def add_class_time_picker(form, fields):
    for field in fields:
        form.fields[field].widget.attrs.update(
            {
                "class": 'form-control timepicker timepicker-default edited',
                'placeholder': '00:00:00'
            }
        )
def add_class_select2(form, fields):
    for field in fields:
        form.fields[field].widget.attrs.update(
            {
                "class": 'form-control select2',
            }
        )


def render_email_template(template_name, context=None):
    return get_template(template_name).render(context)

def html_pdf(request,postulate_pk):
    postulate=Postulate.objects.get(pk=postulate_pk)
    from datetime import date
 
    hoy = date.today()
    fin=get_expirate_date(hoy)
    params = {
        'postulate':postulate,
        'request': request,
        'inicio':hoy,
        'fin':fin,
        'static_url':STATIC_URL,
        'base':BASE_DIR,
        'url':get_current_site(request).domain,
    }
    return Render.render_to_file('main/workers/contract_format.html', params)

def html_pdf_property(request,property_pk):
    try:
        p=Property.objects.get(pk=property_pk)
    except Property.DoesNotExist:
        return Http404
    pd=PropertyDetail.objects.filter(property_fk=p).first()
    params = {
        'p':p,
        'pd':pd,
        'request': request,
        'inicio':hoy,
        'fin':fin,
        'static_url':STATIC_URL,
        'base':BASE_DIR,
        'url':get_current_site(request).domain,
    }
    return Render.render('main/properties/reportproperty.html', params)
def get_expirate_date(hoy):
     
    pagos = 6
    nuevo_mes = hoy.month + pagos
    nuevo_year = hoy.year
    nuevo_dia = hoy.day
     
    if nuevo_mes > 12:
        nuevo_mes = nuevo_mes % 12
        nuevo_year += 1
     
    if nuevo_mes == 2 and hoy.day > 28: nuevo_dia = 28
    if hoy.day > 30 and nuevo_mes in (4, 6, 11): nuevo_dia = 30
     
    hoy = hoy.replace(nuevo_year, nuevo_mes, nuevo_dia)
    return hoy
def bad_request(message):
    response = HttpResponse(json.dumps({'message': message}), 
        content_type='application/json')
    response.status_code = 400
    return response
def good_request(message):
    response = HttpResponse(json.dumps({'message': message}), 
        content_type='application/json')
    response.status_code = 200
    return response
class Departament(object):
    def __init__(self, data):
        self.__dict__ = json.loads(data)

def open_file(url_t):
    import urllib.request, json
    with urllib.request.urlopen(url_t) as url:
        data = json.loads(url.read().decode())
    return data
