"""src URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from panel import views

urlpatterns = [
    url(r'^configuration/$',views.configuration,name='configuration'),
    url(r'^configuration/edit/stats/$',views.configuration_stats,name='configuration_stats'),
    url(r'^configuration/edit/rss/$',views.configuration_rss,name='configuration_rss'),
    url(r'^slider/list/$',views.slider_list,name='slider_list'),
    url(r'^slider/new/$',views.slider_new,name='slider_new'),
    url(r'^slider/edit/(?P<slider_pk>.+)/$',views.slider_edit,name='slider_edit'),
    url(r'^slider/delete/(?P<slider_pk>.+)/$',views.slider_delete,name='slider_delete'),
	url(r'^service/list/$',views.service_list,name='service_list'),
    url(r'^service/new/$',views.service_new,name='service_new'),
    url(r'^service/edit/(?P<service_pk>.+)/$',views.service_edit,name='service_edit'),
    url(r'^service/delete/(?P<service_pk>.+)/$',views.service_delete,name='service_delete'),
    url(r'^service/detail/(?P<service_pk>.+)/$',views.service_detail,name='service_detail'),
	url(r'^pilar/list/$',views.pilar_list,name='pilar_list'),
    url(r'^pilar/new/$',views.pilar_new,name='pilar_new'),
    url(r'^pilar/edit/(?P<pilar_pk>.+)/$',views.pilar_edit,name='pilar_edit'),
    url(r'^pilar/delete/(?P<pilar_pk>.+)/$',views.pilar_delete,name='pilar_delete'),
    url(r'^testimonio/list/$',views.testimony_list,name='testimony_list'),
    url(r'^testimonio/new/$',views.testimony_new,name='testimony_new'),
    url(r'^testimonio/edit/(?P<testimony_pk>.+)/$',views.testimony_edit,name='testimony_edit'),
    url(r'^testimonio/delete/(?P<testimony_pk>.+)/$',views.testimony_delete,name='testimony_delete'),
    url(r'^$',views.team_list,name='team_list'),
    url(r'^equipo/new/$',views.team_new,name='team_new'),
    url(r'^equipo/edit/(?P<team_pk>.+)/$',views.team_edit,name='team_edit'),
    url(r'^equipo/delete/(?P<team_pk>.+)/$',views.team_delete,name='team_delete'),
    url(r'^option/list/$',views.option_list,name='option_list'),
    url(r'^option/new/$',views.option_new,name='option_new'),
    url(r'^option/edit/(?P<option_pk>.+)/$',views.option_edit,name='option_edit'),
    url(r'^option/delete/(?P<option_pk>.+)/$',views.option_delete,name='option_delete'),
    url(r'^post/list/$',views.post_list,name='post_list'),
    url(r'^post/new/$',views.post_new,name='post_new'),
    url(r'^post/edit/(?P<post_pk>.+)/$',views.post_edit,name='post_edit'),
    url(r'^post/delete/(?P<post_pk>.+)/$',views.post_delete,name='post_delete'),
    url(r'^comment/list/(?P<post_pk>.+)/$',views.comment_list,name='comment_list'),
    url(r'^comment/new/(?P<post_pk>.+)/$',views.comment_new,name='comment_new'),
    url(r'^comment/edit/(?P<comment_pk>.+)/$',views.comment_edit,name='comment_edit'),
    url(r'^comment/delete/(?P<comment_pk>.+)/$',views.comment_delete,name='comment_delete'),
    url(r'^comment/child/list/(?P<comment_pk>.+)/$',views.comment_child_list,name='comment_child_list'),
    url(r'^comment/child/(?P<comment_pk>.+)/$',views.comment_child,name='comment_child'),
    url(r'^objetvie/new/(?P<course_pk>.+)/$',views.objetive_new,name='objetive_new'),
    url(r'^objetvie/edit/(?P<course_pk>.+)/(?P<objetive_pk>.+)/$',views.objetive_edit,name='objetive_edit'),
    url(r'^objetvie/delete/(?P<course_pk>.+)/(?P<objetive_pk>.+)/$',views.objetive_delete,name='objetive_delete'),
    #Atentiton hours
    url(r'^atention/new/$',views.atention_new,name='atention_new'),
    url(r'^atention/edit/(?P<atention_pk>.+)/$',views.atention_edit,name='atention_edit'),
    url(r'^atention/delete/(?P<atention_pk>.+)/$',views.atention_delete,name='atention_delete'),
    #Contact Detail
    url(r'^contact/new/$',views.contact_new,name='contact_new'),
    url(r'^contact/edit/(?P<contact_pk>.+)/$',views.contact_edit,name='contact_edit'),
    url(r'^contact/delete/(?P<contact_pk>.+)/$',views.contact_delete,name='contact_delete'),
    #Location Detail
    url(r'^location/new/$',views.location_new,name='location_new'),
    url(r'^location/edit/(?P<location_pk>.+)/$',views.location_edit,name='location_edit'),
    url(r'^location/delete/(?P<location_pk>.+)/$',views.location_delete,name='location_delete'),
]
