from django import forms
from django.forms import ModelForm, HiddenInput, CharField, TextInput
from panel.models import *
from django.utils.translation import ugettext, ugettext_lazy as _
from panel.functions import add_form_control_class, add_field_control_class,add_field_select_class


class SliderForm(ModelForm):
    class Meta:
        model = Slider
        exclude = ['created_at','updated_at']

    def __init__(self, *args, **kwargs):
        super(SliderForm, self).__init__(*args, **kwargs)
        _instance = kwargs.pop('instance', None)
        add_form_control_class(self.fields)
class ServiceForm(ModelForm):
    class Meta:
        model = Service
        exclude = ['created_at','updated_at']

    def __init__(self, *args, **kwargs):
        super(ServiceForm, self).__init__(*args, **kwargs)
        _instance = kwargs.pop('instance', None)
        add_form_control_class(self.fields)
class PilarForm(ModelForm):
    class Meta:
        model = Pilar
        exclude = ['created_at','updated_at']

    def __init__(self, *args, **kwargs):
        super(PilarForm, self).__init__(*args, **kwargs)
        _instance = kwargs.pop('instance', None)
        add_form_control_class(self.fields)
class TestimonyForm(ModelForm):
    class Meta:
        model = Testimony
        exclude = ['created_at','updated_at']

    def __init__(self, *args, **kwargs):
        super(TestimonyForm, self).__init__(*args, **kwargs)
        _instance = kwargs.pop('instance', None)
        add_form_control_class(self.fields)
class TeamForm(ModelForm):
    class Meta:
        model = Team
        exclude = ['created_at','updated_at']

    def __init__(self, *args, **kwargs):
        super(TeamForm, self).__init__(*args, **kwargs)
        _instance = kwargs.pop('instance', None)
        add_form_control_class(self.fields)
class OptionForm(ModelForm):
    class Meta:
        model = OptionChoise
        exclude = ['created_at','updated_at']

    def __init__(self, *args, **kwargs):
        super(OptionForm, self).__init__(*args, **kwargs)
        _instance = kwargs.pop('instance', None)
        add_form_control_class(self.fields)
class PostForm(ModelForm):
    class Meta:
        model = Post
        exclude = ['created_at','updated_at']

    def __init__(self, *args, **kwargs):
        super(PostForm, self).__init__(*args, **kwargs)
        _instance = kwargs.pop('instance', None)
        add_form_control_class(self.fields)
class CourseForm(ModelForm):
    class Meta:
        model = Course
        exclude = ['service','created_at','updated_at']

    def __init__(self, *args, **kwargs):
        super(CourseForm, self).__init__(*args, **kwargs)
        _instance = kwargs.pop('instance', None)
        add_form_control_class(self.fields)
class CourseObjetiveForm(ModelForm):
    class Meta:
        model = CourseObjetive
        exclude = ['course','created_at','updated_at']

    def __init__(self, *args, **kwargs):
        super(CourseObjetiveForm, self).__init__(*args, **kwargs)
        _instance = kwargs.pop('instance', None)
        add_form_control_class(self.fields)
class ConfigurationForm(ModelForm):
    class Meta:
        model = Configuration
        exclude = ['created_at','updated_at']

    def __init__(self, *args, **kwargs):
        super(ConfigurationForm, self).__init__(*args, **kwargs)
        _instance = kwargs.pop('instance', None)
        add_form_control_class(self.fields)
class ConfigurationRSSForm(ModelForm):
    class Meta:
        model = Configuration
        fields = ('linkedin','facebook','instagram','mail','youtube','twitter',)

    def __init__(self, *args, **kwargs):
        super(ConfigurationRSSForm, self).__init__(*args, **kwargs)
        _instance = kwargs.pop('instance', None)
        add_form_control_class(self.fields)

ConfigurationRSSForm
class CommentForm(ModelForm):
    class Meta:
        model = Comment
        exclude = ['post','father','created_at','updated_at']

    def __init__(self, *args, **kwargs):
        super(CommentForm, self).__init__(*args, **kwargs)
        _instance = kwargs.pop('instance', None)
        add_form_control_class(self.fields)

class AtentionHoursForm(ModelForm):
    class Meta:
        model = AtentionHours
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(AtentionHoursForm, self).__init__(*args, **kwargs)
        _instance = kwargs.pop('instance', None)
        add_form_control_class(self.fields)
class ContactDetailForm(ModelForm):
    class Meta:
        model = ContactDetail
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(ContactDetailForm, self).__init__(*args, **kwargs)
        _instance = kwargs.pop('instance', None)
        add_form_control_class(self.fields)
class LocationDetailForm(ModelForm):
    class Meta:
        model = LocationDetail
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(LocationDetailForm, self).__init__(*args, **kwargs)
        _instance = kwargs.pop('instance', None)
        add_form_control_class(self.fields)
