from django.db import models
from django.utils.translation import ugettext as _

# Create your models here.
class Slider(models.Model):
	title=models.CharField(_('Titulo'), max_length=255, blank=False, null=False)
	subtitle=models.TextField(_('Descripcion'),blank=True,null=True)
	is_imagen=models.BooleanField(_('Es imagen? (No marcar si va a subir un video)'),default=True)
	file_data = models.FileField(_('Multimedia'), upload_to="sliders/", null=True)
	is_active=models.BooleanField(_('Activo'),default=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
# Create your models here.
class Service(models.Model):
	name=models.CharField(_('Nombre'), max_length=255, blank=False, null=False)
	title=models.CharField(_('Titulo'), max_length=255, blank=True, null=True)
	subtitle=models.TextField(_('Descripcion'),blank=True,null=True)
	is_imagen=models.BooleanField(_('Es imagen? (No marcar si va a subir un video)'),default=True)
	file_data = models.FileField(_('Multimedia'), upload_to="services/", null=True,blank=True)
	is_imagen_responsive=models.BooleanField(_('Es imagen responsiva? (No marcar si va a subir un video)'),default=True)
	file_data_responsive = models.FileField(_('Multimedia Responsiva'), upload_to="services/", null=True,blank=True)
	icon = models.FileField(_('Icono'), upload_to="services/", null=True,blank=True)
	is_active=models.BooleanField(_('Activo'),default=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	def __str__(self):
		return self.name
class Course (models.Model):
	service=models.ForeignKey(Service,null=False,blank=False)
	name=models.CharField(_('Titulo(*)'), max_length=255, blank=False, null=False)
	description=models.TextField(_('Descripcion Corta(*)'),blank=False,null=False)
	description_long=models.TextField(_('Descripcion Larga(*)'),blank=False,null=False)
	file_metodology = models.FileField(_('Imagen Metodologia'), upload_to="metodology/", null=True,blank=True)
	metodology_title=models.CharField(_('Titulo de Metodologia(*)'), max_length=255, blank=False, null=False)
	metodology=models.TextField(_('Metodologia'),blank=True,null=True)
	objetive_slogan=models.TextField(_('Objetivo Slogan'),blank=True,null=True)
	file_data = models.FileField(_('Objetivo Imagen'), upload_to="objetivo/", null=True)
	price=models.IntegerField(_('Precio'),null=True,blank=True)
	duration=models.CharField(_('Duracion(*)'), max_length=255, blank=False, null=False)
	file_time = models.FileField(_('Imagen tiempo'), upload_to="time/", null=True,blank=True)
	times_slogan=models.TextField(_('Horario Slogan'),blank=True,null=True)
	times=models.TextField(_('Horarios'),blank=True,null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
class CourseObjetive(models.Model):
	course=models.ForeignKey(Course,null=False,blank=False)
	objetive=models.TextField(_('Objetivo'),blank=False,null=False)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

class Pilar(models.Model):
	title=models.CharField(_('Titulo'), max_length=255, blank=True, null=True)
	subtitle=models.CharField(_('Sub Titulo'), max_length=255, blank=True, null=True)
	file_data = models.FileField(_('Multimedia'), upload_to="pilar/", null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
class Testimony(models.Model):
	title = models.CharField(_('Titulo Video'),max_length=255, null=True,blank=True)
	file_data = models.FileField(_('Testimonio'), upload_to="testimonio/", null=True,blank=True)
	video_url = models.CharField(_('Video Url'),max_length=255, null=True,blank=True)
	is_primary=models.BooleanField(_('Principal'),default=False)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	def __str__(self):
		return str(self.id)
class Team(models.Model):
	name=models.CharField(_('Nombre'), max_length=255, blank=False, null=False)
	grade=models.CharField(_('Grado'), max_length=255, blank=False, null=False)
	show_grade=models.BooleanField('Mostrar Grado',default=True)
	testimony=models.TextField(_('Testimonio'),blank=True,null=True)
	linkedin=models.CharField(_('Linkedin'), max_length=255, blank=True, null=True)
	facebook=models.CharField(_('Facebook'), max_length=255, blank=True, null=True)
	instagram=models.CharField(_('Instagram'), max_length=255, blank=True, null=True)
	mail=models.CharField(_('Mail'), max_length=255, blank=True, null=True)
	twitter=models.CharField(_('Twitter'), max_length=255, blank=True, null=True)
	file_data = models.FileField(_('Foto'), upload_to="perfil/", null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name
class OptionChoise(models.Model):
	title=models.CharField(_('Titulo'), max_length=255, blank=False, null=False)
	subtitle=models.TextField(_('Descripcion'),blank=True,null=True)
	file_data = models.FileField(_('Multimedia'), upload_to="opcionchoice/", null=True)
	is_active=models.BooleanField(_('Activo'),default=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
class Configuration(models.Model):
	cases_success=models.IntegerField(_('Casos de Exito'),null=True,blank=True, default="0")
	actual_clients=models.IntegerField(_('Clientes Actuales'),null=True,blank=True ,default="0")
	post_spain=models.IntegerField(_('Programas de Posgrado en Espania'),null=True,blank=True,default="0")
	aprovedd_tesis=models.IntegerField(_('Tesis Aprobadas'),null=True,blank=True,default="0")
	first_slider = models.FileField(_('Primer Multimedia'), upload_to="sliders/", null=True)
	title_first=models.CharField(_('Titulo'), max_length=255, blank=False, null=False)
	subtitle_first=models.TextField(_('Descripcion'),blank=True,null=True)
	form_title=models.CharField(_('Formulario Titulo'), max_length=255, blank=True, null=True)
	show_staff=models.BooleanField(_('Mostrar Staff'),default=True)
	contact_content=models.TextField(_('Contacto Contenido'),blank=True,null=True)
	mision=models.TextField(_('Mision'),blank=True,null=True)
	vision=models.TextField(_('Vision'),blank=True,null=True)
	linkedin=models.CharField(_('Linkedin'), max_length=255, blank=True, null=True)
	facebook=models.CharField(_('Facebook'), max_length=255, blank=True, null=True)
	instagram=models.CharField(_('Instagram'), max_length=255, blank=True, null=True)
	mail=models.CharField(_('Mail'), max_length=255, blank=True, null=True)
	youtube=models.CharField(_('Youtube'), max_length=255, blank=True, null=True)
	twitter=models.CharField(_('Twitter'), max_length=255, blank=True, null=True)
class Post(models.Model):
	title=models.CharField(_('Titulo'), max_length=255, blank=False, null=False)
	category=models.CharField(_('Categoria'), max_length=255, blank=False, null=False)
	content=models.TextField(_('Contenido'),blank=False,null=False)
	file_data = models.FileField(_('Multimedia'), upload_to="post/", null=False)
	file_icon = models.FileField(_('Multimedia Home'), upload_to="post/", null=False)
	author=models.ForeignKey(Team,null=True,blank=True)
	is_active=models.BooleanField(_('Activo'),default=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	def __str__(self):
		return str(self.pk)

class Comment(models.Model):
	post=models.ForeignKey(Post,null=False,blank=False)
	name=models.CharField(_('Ingrese su nombre'), max_length=255, blank=False, null=False)
	email=models.EmailField(_('E-mail'),blank=False,null=False)
	comment=models.TextField(_('Comentario'),blank=False,null=False)
	father=models.ForeignKey(to='Comment',related_name='Comment',null=True,blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	def __str__(self):
		return str(self.pk)

class AtentionHours(models.Model):
	title=models.CharField(_('Dias'), max_length=255, blank=False, null=False)
	hours=models.TextField(_('Horarios'),blank=False,null=False)

class ContactDetail(models.Model):
	title=models.CharField(_('Titulo'), max_length=255, blank=False, null=False)
	content=models.TextField(_('Contenido'),blank=False,null=False)

class LocationDetail(models.Model):
	title=models.CharField(_('Titulo'), max_length=255, blank=False, null=False)
	address=models.TextField(_('Direccion'),blank=False,null=False)
	map_embeded=models.TextField(_('Google Map'),blank=False,null=False)


